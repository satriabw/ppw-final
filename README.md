# SULING
[![pipeline status](https://gitlab.com/PPL2018csui/Kelas-C/ppw-final/badges/sit_uat/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-C/ppw-final/commits/sit_uat) ![coverage](https://gitlab.com/PPL2018csui/Kelas-C/ppw-final/badges/sit_uat/coverage.svg?job=be_tests) ![coverage](https://gitlab.com/PPL2018csui/Kelas-C/ppw-final/badges/sit_uat/coverage.svg?job=fe_tests_v1)

The lack of information and data about Faculty of Computer Science alumni became the main problem of this project, which also leads to the lack of relationship between faculty administrative and alumni.

This project aims to record students and alumni data that can be updated easily and periodically, so as to establish connections between faculty and alumni, or between alumni.

With the establishment of good relationship between alumni and faculty, it is expected that students will find opportunities to work with alumni, or build new start-ups with alumni in the same network. In addition, other companies and organizations may register on this platform to enable employment and internships for alumni and faculty students.

>The project title is **SuliNG**. This name was inspired by LingIn, the formerly name of this project, and adopting the way Fasilkom gives name for their administrative applications (SiAsisten, SiakNG). However, the pronunciation of SuliNG is not really like what Indonesian people usually heard, but SuliNG is pronounced like SiakNG’s style.

## Usage

With [foreman](https://github.com/ddollar/foreman) and [project](#install) installed, run

    $ foreman start

## Install

With [node](https://nodejs.org/en/) and [python 3.x.x](https://www.python.org/) installed, run

#### Django REST Backend

OSX & GNU/Linux:

    $ cd backend/v1
    $ python -m venv env
    $ source env/bin/activate
    $ pip3 install -r requirements.txt

>change `pip`, `python` into `pip3`, `python3` for osx

Windows:
`not supported yet, maybe won't`

    $ shutdown -s -t 0

#### Django Frontend

OSX & GNU/Linux:

    $ cd frontend/v1
    $ python -m venv env
    $ source env/bin/activate
    $ pip3 install -r requirements.txt

>change `pip`, `python` into `pip3`, `python3` for osx

Windows:
`not supported yet, maybe won't`

    $ shutdown -s -t 0

#### React Frontend
With [Yarn](https://yarnpkg.com/en/) installed, run

    $ cd frontend/v2
    $ yarn

## Contributor

1. Elvan Rizky Novandi
2. Muhammad Faisal Taufiqur R
3. Muhammad Aulia Adil
4. Ahmad Satryaji Aulia
5. Michael Tengganus
6. Wisnu Pramadhitya Ramadhan
7. Affan Dhia Ardhiva
8. Fachrur Rozi
9. Imam Maulana Rachbini
10. M. Rizal Diantoro
11. Satria Bagus Wicaksono
12. Andhika


## License

MIT
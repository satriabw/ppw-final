from django.core.mail import EmailMessage
from django.conf import settings
from django.core.mail import send_mail


class EmailHelper():

    def send_email(self, email_dest, username, password):
        try:
            body = """
                Thank you have been verified for SULING
                Here are your credentials,
                username: {}
                password: {}
                """.format(username, password)
            email = EmailMessage(
                '[SULING] Confirmation Mail',
                body,
                to=[email_dest]
            )
            email.send()
        except Exception:
            raise Exception("Email is not sent")

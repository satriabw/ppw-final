import json
from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from app_registration.views import SignUpAPIView
from django.contrib.auth.models import User
from app_registration.models import UserRegistration
from app_registration.serializers import UserRegistrationSerializer
from datetime import date
from .utils import EmailHelper
from django.test import TestCase
from mock import patch, Mock

# Create your tests here.


class SignUpAPIViewTestCase(APITestCase):

    def setUp(self):
        self.username = 'fachrurRz'
        self.password = 'inipasss'
        self.email = 'fachrur@suling.com'

        self.user = User.objects.create_user(username=self.username,
                                             password=self.password,
                                             email=self.email)

        self.url = reverse('register:signup')

    def test_signup_accept(self):
        payload = {
            "username": "fachrur.rozi",
            "email": "fachrur@iui.ac.id",
            "name": "Fachrur Rozi",
            "birthday": "1997-02-02",
            "angkatan": 2015,
            "npm": "1506689143",
            "pa_name": "Bob Hardian"
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(201, response.status_code)

    def test_signup_fail_with_exist_email(self):
        payload = {
            "username": "fachrur.rozi",
            "email": self.email,
            "name": "Fachrur Rozi",
            "birthday": "1997-02-02",
            "angkatan": 2015,
            "npm": "1506689143",
            "pa_name": "Bob Hardian"
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(400, response.status_code)

    def test_signup_fail_with_exist_username(self):
        payload = payload = {
            "username": self.username,
            "email": "fachrur@iui.ac.id",
            "name": "Fachrur Rozi",
            "birthday": "1997-02-02",
            "angkatan": 2015,
            "npm": "1506689143",
            "pa_name": "Bob Hardian"
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(400, response.status_code)

    def test_signup_fail_with_bad_request(self):
        payload = {
            "unknown": self.username,
            "email": "fachrur@ui.ac.id",
            "password": self.password,
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(400, response.status_code)


class EmailHelperTestCase(TestCase):
    def setUp(self):
        self.helper = EmailHelper()

    @patch('app_registration.utils.EmailMessage.send')
    def test_send_email(self, mock_email):
        # test method
        self.helper.send_email(
            'g1831894@nwytg.com',
            'johnsnow',
            'you_know_nothing'
        )
        self.assertTrue(mock_email.called)

    @patch('app_registration.utils.EmailMessage.send',
           Mock(side_effect=Exception('Email is not sent')))
    def test_send_email_fail(self):
        with self.assertRaises(Exception) as e:
            self.helper.send_email(
                'thisisnotemail',
                'johnsnow',
                'you_know_nothing'
            )

        message = str(e.exception)
        self.assertEqual(message, "Email is not sent")


class UserRegistrationListTestCase(APITestCase):
    def setUp(self):
        self.username = 'imam'
        self.password = 'asdasdasd'
        self.user_admin = User.objects.create_superuser(
            username=self.username,
            email='imam@rachbini.com',
            password=self.password,
        )

        self.username_biasa = 'biasa'
        self.user_biasa = User.objects.create_user(
            username=self.username_biasa,
            password=self.password,
        )

        payload = {
            "username": "fachrur.rozi",
            "email": "fachrur@iui.ac.id",
            "name": "Fachrur Rozi",
            "birthday": "1997-02-02",
            "angkatan": 2015,
            "npm": "1506689143",
            "pa_name": "Bob Hardian"
        }
        self.client.post(
            reverse('register:signup'),
            data=payload,
        )

    def test_view_user_registration_list(self):
        self.client.login(
            username=self.username,
            password=self.password,
        )

        response = self.client.get('/register/list/')
        self.assertEqual(200, response.status_code)

    def test_view_user_registration_list_user_non_admin(self):
        self.client.login(
            username=self.username_biasa,
            password=self.password,
        )

        response = self.client.get('/register/list/')
        self.assertEqual(403, response.status_code)

    def test_view_user_registration_list_without_login(self):
        response = self.client.get('/register/list/')
        self.assertEqual(401, response.status_code)

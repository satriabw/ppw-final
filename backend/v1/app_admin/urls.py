from django.conf.urls import url, include
from app_admin.views import (
    AdminDashboard,
    AdminRegistrationMahasiswaList,
    AdminVerifyMahasiswa,
)

urlpatterns = [
    url(r'^dashboard/$',
        AdminDashboard.as_view(),
        name="dashboard"),
    url(r'^registration/mahasiswa/$',
        AdminRegistrationMahasiswaList.as_view(),
        name="regist-mhs"),
    url(r'^registration/mahasiswa/(?P<pk>[0-9]+)/$',
        AdminVerifyMahasiswa.as_view(),
        name="verify-mhs"),
]

from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework import status
from django.db.models import Q

from app_admin.models import (
    User,
    UserMahasiswa,
    UserPerusahaan,
    UserRegistration,
    UserProfile,
    Job,
)
from app_admin.serializers import UserRegistrationSerializer
from app_registration.utils import EmailHelper


class AdminDashboard(APIView):
    permission_classes = (
        IsAuthenticated,
        IsAdminUser,
    )

    def get(self, request, *args, **kwargs):
        data = {
            'mahasiswa_sum': UserMahasiswa.objects.count(),
            'perusahaan_sum': UserPerusahaan.objects.count(),
            'mahasiswa_unverified_sum': UserRegistration.objects.count(),
            'job_sum': Job.objects.count(),
        }

        return Response(data)


class AdminRegistrationMahasiswaList(ListAPIView):
    queryset = UserRegistration.objects.all()
    serializer_class = UserRegistrationSerializer
    permission_classes = (
        IsAuthenticated,
        IsAdminUser,
    )


class AdminVerifyMahasiswa(APIView):
    permission_classes = (
        IsAuthenticated,
        IsAdminUser,
    )

    def get(self, request, pk, format=None):
        try:
            user_regist = UserRegistration.objects.get(id=pk)
            data = UserRegistrationSerializer(user_regist).data
            return Response(data, status.HTTP_200_OK)
        except UserRegistration.DoesNotExist:
            return Response({'message': 'user registration not found'}, 404)

    def post(self, request, pk, format=None):
        try:
            user_regist = UserRegistration.objects.get(id=pk)
            username = user_regist.username
            name = user_regist.name
            birthday = user_regist.birthday
            email = user_regist.active_email
            angkatan = user_regist.angkatan
            npm = user_regist.npm
            password = User.objects.make_random_password()

            user = User.objects.filter(Q(username=username) |
                                       Q(email=email))

            if len(user) > 0:

                message = {
                    "data": {
                        "message":
                            "user with that username or email already exist",
                    }
                }

                return Response(message, status=status.HTTP_400_BAD_REQUEST)

            user = User.objects.create_user(
                username=username,
                email=email,
                password=password,
            )

            user_mahasiswa = UserMahasiswa.objects.create(
                id=user.id,
                user=user,
                nama_lengkap=name,
                npm=npm,
                angkatan=angkatan,
                is_valid=True,
            )

            user_profile = UserProfile.objects.create(
                id=user.id,
                user_mahasiswa=user_mahasiswa,
                secondary_email=None,
                phone_no=None,
                profpic_url=None,
            )

            # mail = EmailHelper()
            # mail.send_email(email, username, password)

            user_regist.delete()

            return Response(
                {'message': 'user verified'},
                status.HTTP_201_CREATED
            )

        except UserRegistration.DoesNotExist:
            return Response(
                {'message': 'user registration not found'},
                status.HTTP_404_NOT_FOUND
            )

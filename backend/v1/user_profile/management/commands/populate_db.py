from datetime import date
from django.core.management.base import BaseCommand
from app_auth.models import (
    User,
    UserMahasiswa,
    UserPerusahaan,
)
from user_profile.models import (
    UserProfile,
    Experience,
    Skill,
)
from lowongan.models import (
    Perusahaan,
    Job,
)
from app_registration.models import (
    UserRegistration,
)


class Command(BaseCommand):

    def _create_admin(self):
        User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='adminadmin',
        )

    def _create_user_profiles(self):
        user = User.objects.create_user(
            username='john',
            email='john@snow.com',
            password='you_know_nothing',
        )

        student = UserMahasiswa.objects.create(
            id=user.id,
            user=user,
            nama_lengkap="John Snow",
            npm="1012345678",
            angkatan="2010",
            is_valid=True,
        )

        profile = UserProfile.objects.create(
            id=student.id,
            user_mahasiswa=student,
            subtitle='King of the North',
            location='Winterfell',
            secondary_email='john_snow@winterfell.com',
            phone_no='08121212884',
            profpic_url='http://www.img.com',
        )

        experience11 = Experience.objects.create(
            user_profile=profile,
            title='King of the North',
            organization='Winterfell',
            description='I am the King',
            start_date=date(2010, 1, 1),
            end_date=date(2015, 12, 31),
            category='job',
        )

        experience12 = Experience.objects.create(
            user_profile=profile,
            title='Dragon Trainer',
            organization='Winterfell',
            description='I am good with dragons.',
            start_date=date(2010, 1, 1),
            end_date=date(2015, 12, 31),
            category='job',
        )

        skill11 = Skill.objects.create(
            user_profile=profile,
            title='War',
        )

        skill12 = Skill.objects.create(
            user_profile=profile,
            title='Dragon Ride',
        )

        user2 = User.objects.create_user(
            username='arya',
            email='arya@stark.com',
            password='you_know_nothing',
        )

        student2 = UserMahasiswa.objects.create(
            id=user2.id,
            user=user2,
            nama_lengkap="Arya Stark",
            npm="1312345678",
            angkatan="2013",
            is_valid=True,
        )

        profile2 = UserProfile.objects.create(
            id=student2.id,
            user_mahasiswa=student2,
            subtitle='Faceless Men',
            location='Nowhere',
            secondary_email='arya_stark@winterfell.com',
            phone_no='08121212884',
            profpic_url='http://www.img.com',
        )

        experience21 = Experience.objects.create(
            user_profile=profile2,
            title='Faceless Man',
            organization='Winterfell',
            description='I am untracked',
            start_date=date(2013, 1, 1),
            end_date=date(2018, 12, 31),
            category='job',
        )

        experience22 = Experience.objects.create(
            user_profile=profile2,
            title='Executioner',
            organization='Winterfell',
            description='Mind you head.',
            start_date=date(2013, 1, 1),
            end_date=date(2018, 12, 31),
            category='job',
        )

        skill21 = Skill.objects.create(
            user_profile=profile2,
            title='Disguise',
        )

        skill22 = Skill.objects.create(
            user_profile=profile2,
            title='Sword Fight',
        )

    def _create_perusahaan(self):
        self.perusahaan1 = Perusahaan.objects.create(
            name='Traveloka',
            company_logo='http://asd.com/',
            description='Terbang Kuy',
            address='Slipicon Valley',
            contact_person='hrd@traveloka.com',
        )

        self.perusahaan2 = Perusahaan.objects.create(
            name='Tokopedia',
            company_logo='http://asd.com/',
            description='Belanja Kuy',
            address='Kuningan',
            contact_person='hrd@tokopedia.com',
        )

    def _create_jobs(self):
        self.job11 = Job.objects.create(
            perusahaan=self.perusahaan1,
            position='Backend Engineer',
            location='Slipicon Valley',
            description='Ngoding Kuy',
        )

        self.job12 = Job.objects.create(
            perusahaan=self.perusahaan1,
            position='Frontend Engineer',
            location='Slipicon Valley',
            description='Design Kuy',
        )

        self.job21 = Job.objects.create(
            perusahaan=self.perusahaan2,
            position='Backend Engineer',
            location='Kuningan',
            description='Ngoding Kuy',
        )

        self.job22 = Job.objects.create(
            perusahaan=self.perusahaan2,
            position='Frontend Engineer',
            location='Kuningan',
            description='Design Kuy',
        )

    def _create_user_registration(self):
        regis1 = UserRegistration.objects.create(
            username='john_boyega',
            name='John Boyega',
            birthday=date(1990, 12, 12),
            active_email='john@boyega.com',
            angkatan=2008,
            npm='08091234123',
            pa_name='Bennedict Cumberbatch',
        )

        regis2 = UserRegistration.objects.create(
            username='tony_stark',
            name='Tony Stark',
            birthday=date(1980, 12, 12),
            active_email='tony@stark.com',
            angkatan=1998,
            npm='9812345678',
            pa_name='Sherlock Holmes',
        )

        regis3 = UserRegistration.objects.create(
            username='j_depp',
            name='Johnny Depp',
            birthday=date(1985, 11, 5),
            active_email='johhny@depp.com',
            angkatan=2003,
            npm='0312435412',
            pa_name='Brad Pitt',
        )

    def handle(self, *args, **options):
        self._create_admin()
        self._create_user_profiles()
        self._create_perusahaan()
        self._create_jobs()
        self._create_user_registration()

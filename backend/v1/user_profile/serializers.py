from rest_framework import serializers

from user_profile.models import (
    UserProfile,
    Experience,
    Skill,
    UserLinkedinProfile,
    UserCSAuthProfile,
)
from app_auth.serializers import UserMahasiswaSerializer


class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = (
            "id",
            "user_profile",
            "title",
            "organization",
            "organization_logo_url",
            "description",
            "start_date",
            "end_date",
            "category",
        )


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = (
            "id",
            "user_profile",
            "title"
        )


class UserProfileSerializer(serializers.ModelSerializer):
    experiences = ExperienceSerializer(read_only=True, many=True)
    skills = SkillSerializer(read_only=True, many=True)
    user_mahasiswa = UserMahasiswaSerializer(read_only=True)

    class Meta:
        model = UserProfile
        fields = (
            'id',
            'user_mahasiswa',
            'subtitle',
            'location',
            'secondary_email',
            'phone_no',
            'profpic_url',
            'experiences',
            'skills',
        )


class UserCSAuthProfileSerializer(serializers.ModelSerializer):

    user_mahasiswa = UserMahasiswaSerializer(read_only=True)

    class Meta:
        model = UserCSAuthProfile
        fields = (
            'user_mahasiswa',
            'alamat',
            'kota_lahir',
            'tanggal_lahir',
            'prodi',
            'program',
        )


class UserLinkedinProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserLinkedinProfile
        fields = '__all__'

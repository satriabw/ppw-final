from django.db import models
from django.contrib.auth.models import User
from app_auth.models import UserMahasiswa


class UserProfile(models.Model):
    user_mahasiswa = models.OneToOneField(
        UserMahasiswa,
        on_delete=models.CASCADE,
        related_name='user_profile',
    )
    subtitle = models.CharField(max_length=128, blank=True, null=True)
    location = models.CharField(max_length=128, blank=True, null=True)
    secondary_email = models.CharField(max_length=128, blank=True, null=True)
    phone_no = models.CharField(max_length=128, blank=True, null=True)
    profpic_url = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.user_mahasiswa.nama_lengkap


class Experience(models.Model):
    CATEGORY_CHOICES = (
        ('EDUCATION', 'education'),
        ('JOB', 'job'),
        ('VOLUNTEER', 'volunteer'),
    )

    user_profile = models.ForeignKey(UserProfile, related_name='experiences')
    title = models.CharField(max_length=140)
    organization = models.CharField(max_length=140)
    organization_logo_url = models.CharField(
        max_length=500,
        blank=True,
        null=True
    )
    description = models.CharField(max_length=140, blank=True, null=True)
    start_date = models.DateField()
    end_date = models.DateField()
    category = models.CharField(max_length=10, choices=CATEGORY_CHOICES)

    def __str__(self):
        return self.title + ' at ' + self.organization


class Skill(models.Model):

    user_profile = models.ForeignKey(UserProfile, related_name='skills')
    title = models.CharField(max_length=140)

    def __str__(self):
        return self.title


class UserLinkedinProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=48, null=True)
    last_name = models.CharField(max_length=48, null=True)
    headline = models.CharField(max_length=200, null=True)
    location = models.CharField(max_length=200, null=True)
    industry = models.CharField(max_length=128, null=True)
    current_share = models.IntegerField(null=True)
    num_connections = models.IntegerField(null=True)
    summary = models.TextField(max_length=500, null=True)
    specialties = models.TextField(max_length=300, null=True)
    picture_url = models.URLField(null=True)
    public_profile_url = models.URLField(null=True)
    email_address = models.EmailField(null=True)

    def __str__(self):
        return self.first_name


class UserCSAuthProfile(models.Model):
    user_mahasiswa = models.OneToOneField(
        UserMahasiswa,
        on_delete=models.CASCADE
    )
    alamat = models.CharField(max_length=250, null=True)
    kota_lahir = models.CharField(max_length=50, null=True)
    tanggal_lahir = models.DateField(null=True)
    prodi = models.CharField(max_length=100, null=True)
    program = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.user_mahasiswa.nama_lengkap

from django.contrib import admin

# Register your models here.
from user_profile.models import (
    UserProfile,
    Experience,
    Skill,
    UserCSAuthProfile
)


admin.site.register(UserProfile)
admin.site.register(Experience)
admin.site.register(Skill)
admin.site.register(UserCSAuthProfile)

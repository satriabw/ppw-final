from django.conf import settings
from django.contrib.auth.models import User
from user_profile.models import (
    UserLinkedinProfile,
    UserCSAuthProfile
)
from rest_framework_jwt.settings import api_settings
from app_auth.models import UserMahasiswa
import requests
import datetime
import random
import string
import os

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

EMAIL_DOMAIN = '@ui.ac.id'


class AuthHelper:

    def get_access_token(self, username, password):
        try:
            url = settings.API_GET_TOKEN
            authorization = '{} {}'.format(
                "Basic", os.getenv('CSAUTH_TOKEN', ''))
            payload = self.login_payload_builder(username, password)

            headers = {
                'authorization': authorization,
                'cache-control': "no-cache",
                'content-type': "application/x-www-form-urlencoded"
            }
            response = requests.post(url, data=payload, headers=headers)

            return response.json()["access_token"]

        except Exception:
            return ('username or password is wrong')

    def verify_user(self, access_token):
        parameters = {"access_token": access_token,
                      "client_id": os.getenv('CLIENT_ID', '')}
        response = requests.get(
            settings.API_VERIFY_USER, params=parameters)
        return response

    def get_user_data(self, access_token, id):
        url = '{}{}'.format(settings.API_MAHASISWA, id)
        parameters = {"access_token": access_token,
                      "client_id": os.getenv('CLIENT_ID', '')}
        response = requests.get(url, params=parameters)
        return response.json()

    def login_payload_builder(self, username, password):
        return 'username={}&password={}&grant_type=password'.format(
            username,
            password)

    def get_firstname_lastname(self, name):
        firstname = name.split(" ")[0]
        lastname = " ".join(name.split(" ")[1:])

        return firstname, lastname

    def get_angkatan_by_npm(self, npm):
        prefix = '20'
        angkatan = npm[0:2]
        return '{}{}'.format(prefix, angkatan)

    def get_email_by_username(self, username):
        email = '{}{}'.format(username, EMAIL_DOMAIN)

        return email

    def get_prodi_and_program(self, data):
        prodi = data["program"][0]["nm_org"]
        program = data["program"][0]["nm_prg"]
        return (prodi, program)

    def save_user_profile(self, email, first_name, last_name):
        if email is None:
            raise ValueError("Email tidak boleh kosong!")

        password = User.objects.make_random_password()

        user = User()
        user.username = email
        user.email = email
        user.first_name = first_name
        user.last_name = last_name
        user.password = password
        user.save()

        return user

    def save_user_mahasiswa_profile(self,
                                    user,
                                    npm,
                                    fullname,
                                    angkatan,
                                    is_valid=False):

        if user is None:
            raise ValueError("Tidak ada user yang sedang aktif!")

        if fullname is None:
            raise ValueError("Nama lengkap tidak boleh kosong!")

        user_mahasiswa = UserMahasiswa()
        user_mahasiswa.user = user
        user_mahasiswa.npm = npm
        user_mahasiswa.nama_lengkap = fullname
        user_mahasiswa.angkatan = angkatan
        user_mahasiswa.is_valid = is_valid
        user_mahasiswa.save()

        return user_mahasiswa

    def save_user_linkedin_profile(self, user, first_name, last_name,
                                   headline, location, industry,
                                   current_share, num_connections,
                                   summary, specialties, picture_url,
                                   public_profile_url, email_address):
        if user is None:
            raise ValueError("Tidak ada user yang sedang aktif!")

        user_linkedin_profile = UserLinkedinProfile()
        user_linkedin_profile.user = user
        user_linkedin_profile.first_name = first_name
        user_linkedin_profile.last_name = last_name
        user_linkedin_profile.headline = headline
        user_linkedin_profile.location = location
        user_linkedin_profile.industry = industry
        user_linkedin_profile.current_share = current_share
        user_linkedin_profile.num_connections = num_connections
        user_linkedin_profile.summary = summary
        user_linkedin_profile.specialties = specialties
        user_linkedin_profile.picture_url = picture_url
        user_linkedin_profile.public_profile_url = public_profile_url
        user_linkedin_profile.email_address = email_address
        user_linkedin_profile.save()
        return user_linkedin_profile

    def save_user_csauth_profile(
            self,
            user_mahasiswa,
            alamat,
            kota_lahir,
            tanggal_lahir,
            prodi,
            program):
        if user_mahasiswa is None:
            raise ValueError("Tidak ada user yang sedang aktif!")

        user_cs_auth_profile = UserCSAuthProfile()
        user_cs_auth_profile.user_mahasiswa = user_mahasiswa
        user_cs_auth_profile.alamat = alamat
        user_cs_auth_profile.kota_lahir = kota_lahir
        user_cs_auth_profile.tanggal_lahir = tanggal_lahir
        user_cs_auth_profile.prodi = prodi
        user_cs_auth_profile.program = program
        user_cs_auth_profile.save()
        return user_cs_auth_profile


class LinkedinHelper:

    def get_random_state(self):
        random_state = ''.join(
            [random.choice(
                string.ascii_letters + string.digits
            ) for n in range(32)]
        )
        return random_state


class JWTHelper():

    def store_to_cookies(self, response, user, days_expire=3):
        max_age = days_expire * 24 * 60 * 60
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        expires = datetime.datetime.strftime(
            datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age),
            "%a, %d-%b-%Y %H:%M:%S GMT"
        )
        response.set_cookie(
            'jwt_', token, max_age=max_age, expires=expires,
            domain=settings.SESSION_COOKIE_DOMAIN,
            secure=settings.SESSION_COOKIE_SECURE or None
        )
        return response

    def get_jwt(self, user):
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        return token

from lowongan.models import Job, Perusahaan, JobApplication
from user_profile.serializers import UserProfileSerializer
from app_auth.serializers import UserMahasiswaSerializer
from rest_framework import serializers


class PerusahaanSerializer(serializers.ModelSerializer):

    class Meta:
        model = Perusahaan
        fields = (
            "id",
            "name",
            "description",
            "address",
            "contact_person",
            "company_logo"
        )


class JobSerializer(serializers.ModelSerializer):
    perusahaan = PerusahaanSerializer(read_only=True)

    class Meta:
        model = Job
        fields = (
            "id",
            "perusahaan",
            "position",
            "description",
            "created_at",
            "location",
        )


class JobApplicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobApplication
        fields = (
            'job',
            'user',
            'message',
            'cv_url',
            'created_at',
        )

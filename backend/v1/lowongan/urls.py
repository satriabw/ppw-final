from django.conf.urls import url
from lowongan.views import (
    LowonganListAPIView,
    LowonganDetailsAPIView,
    JobApplicationCreateAPIView,
    AplliedJobListAPIView
)


urlpatterns = [
    url(r'^$', LowonganListAPIView.as_view(), name="list"),
    url(r'^(?P<pk>[0-9]+)/$', LowonganDetailsAPIView.as_view(), name="detail"),
    url(r'^apply/$', JobApplicationCreateAPIView.as_view(), name="apply"),
    url(r'^apply/history/$',
        AplliedJobListAPIView.as_view(),
        name="apply-history"),
]

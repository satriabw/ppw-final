# PPW Last Assignment Repository
Aplikasi ini bertujuan untuk digunakan mahasiswa/i Fasilkom maupun alumni dari Fasilkom UI untuk menemukan pekerjaan layaknya Linkedin.
Aplikasi ini juga bertujuan untuk digunakan pemilik perusahaan maupun anggota perusahaan yang ingin mencari mahasiswa/i maupun alumni untuk keperluan job listing
Applikasi ini merupakan penyatuan dari tugas akhir mata kuliah Perancangan & Pemrograman Web yang telah di satuan sedemikian rupa, dan dikembangkan oleh pihak bersangkutan


## Pipelines status
[![pipeline status](https://gitlab.com/satraul/ppw-final/badges/master/pipeline.svg)](https://gitlab.com/satraul/ppw-final/commits/master)

## code coverage status
[![coverage report](https://gitlab.com/satraul/ppw-final/badges/master/coverage.svg)](https://gitlab.com/satraul/ppw-final/commits/master)

## Copyright (c) Fasilkom UI Under MIT License

## Main Contributor 
1. 1606822756   Elvan Rizky Novandi
2. 1606838666   Muhammad Faisal Taufiqur R
3. 1606874671   Muhammad Aulia Adil
4. 1606885864   Ahmad Satryaji Aulia
5. 1606917651   Michael Tengganus
6. 1606918055   Wisnu Pramadhitya Ramadhan
7. 1506689225   Affan Dhia Ardhiva

* * *

## Table of Contents
Welcome to the code repository. This repository hold 

1. app - Holds student and job listing
2. app_auth - Holds authenticating login and logout for apps
3. app_company - Holds company login page
4. app_mahasiswa - Holds mahasiswa login page
5. ppw_final - Holds setting for apps project
6. templates - Holds templates for apps project

## Initial Setup
Assume you already have django and git installed. Do the following step:

in Command Prompt:
1. git clone https://gitlab.com/satraul/ppw-final.git | in your directory
2. python -m venv env | create environment for python

for Windows
3. env\Scripts\activate.bat

for Linux & Mac OS
3. source env/bin/activate

4. pip install -r requirements.txt | install requirements
5. python manage.py makemigrations
6. python manage.py migrate
7. python manage.py runserver 8000
8. open "localhost:8000" in your local browser


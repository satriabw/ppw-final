from django import forms

class JobForm(forms.Form):
    title = forms.CharField(max_length=140)
    description = forms.CharField(max_length=500)
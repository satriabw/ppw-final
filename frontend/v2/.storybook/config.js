import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import Container from './Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../app/global-styles';

// storybook addons
addDecorator(story => <Container story={story} />);

// automatically import all files ending in *.stories.js
const req = require.context('../stories', true, /.stories.js$/);
const globalComponents = require.context('../app/components', true, /\.stories\.js$/);

function loadStories() {
  globalComponents.keys().forEach(filename => globalComponents(filename));
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

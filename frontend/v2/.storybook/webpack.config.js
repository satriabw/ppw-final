const mainConfig = require("../internals/webpack/webpack.dev.babel");

// Export a function. Accept the base config as the only param.
module.exports = function(storybookBaseConfig) {
  storybookBaseConfig.module.rules = storybookBaseConfig.module.rules.concat(
    mainConfig.module.rules
  );
  storybookBaseConfig.resolve.modules = storybookBaseConfig.resolve.modules.concat(
    mainConfig.resolve.modules
  );
  storybookBaseConfig.resolve.modules = storybookBaseConfig.resolve.modules.concat(
    mainConfig.resolve.modules
  );
  return storybookBaseConfig;
};

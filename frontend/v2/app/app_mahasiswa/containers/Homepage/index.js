/**
 *
 * Homepage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import Cookies from 'universal-cookie';

import injectSaga from 'utils/injectSaga';
import authReducer from 'app_auth/containers/Homepage/reducer';
import authSaga from 'app_auth/containers/Homepage/saga';
import injectReducer from 'utils/injectReducer';
import Hero from 'app_mahasiswa/components/Hero';
import Image from 'components/Img';
import Button from 'components/Button';
import CompanyPlaceholderImage from 'images/company_placeholder.png';
import UserPlaceholderImage from 'images/user_placeholder.png';
import { SlPencil, SlClose } from 'components/Icon/sl';
import makeSelectHomepage, { makeSelectAppAuth } from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  getProfileSuling,
  toggleEdit,
  getAuthState,
  editProfileSuling,
} from './actions';
import autoBind from 'react-autobind';

const PIC_SIZE = 7.5;
const OFFSET = 14;
const MAIN_PADDING = 2;

const variables = {
  profilePicSize: `${PIC_SIZE}em`,
  offset: `${OFFSET}em`,
  mainPadding: `${MAIN_PADDING}em`,
  headerMarginTop: `-${PIC_SIZE - MAIN_PADDING}em`,
};

const MainWrapper = styled.div`
  margin-top: -${(props) => props.variables.offset};
  background-color: white;
  border-radius: 0.5em;
  box-shadow: 0 2px 16px rgba(0, 0, 0, 0.1);
  padding: ${(props) => props.variables.mainPadding};
  position: relative;

  .main__header {
    margin-top: ${(props) => props.variables.headerMarginTop};
    font-family: "Open Sans", sans-serif;

    .main__profile-pic {
      img {
        width: ${(props) => props.variables.profilePicSize};
        height: ${(props) => props.variables.profilePicSize};
      }
    }

    .main__name {
      font-weight: 700;
      color: rgba(77, 161, 255, 0.74);
    }

    .main__location {
      font-weight: 400;
      color: #a6a6a6;
    }
  }

  .main__toolbar {
    position: absolute;
    top: 21px;
    right: 21px;

    .main__item {
      font-size: px_to_em(22);
      color: #dde2e7;
      margin: 0 0.5em;
      display: inline;

      &:hover {
        color: black;
        text-decoration: none;
      }
    }
  }

  .main__exp {
    .main__title {
      font-weight: 400;
    }

    .main__item {
      background-color: white;
      box-shadow: 0 2px 16px rgba(0, 0, 0, 0.1);

      .main__logo {
        > img {
          width: ${(props) => props.variables.profilePicSize};
          height: auto;
          border-radius: ${(props) => props.variables.profilePicSize} / 2;
          box-sizing: border-box;
        }
      }

      .main__title {
        font-weight: bolder;
        font-size: px_to_em(24);
      }
      .main__position {
        font-size: px_to_em(18);
        color: #a6a6a6;
      }
      .main__description {
        font-size: small;
      }

      .main__date {
        font-weight: 800;
        font-size: 1.45rem;
      }
    }
  }
`;

export class Homepage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    autoBind(this);
  }

  componentDidMount() {
    const cookie = new Cookies();
    const userId = cookie.get('user_id');
    this.props.onGetProfileSuling(userId);
    this.props.onGetAuthState();
  }

  onToggleEdit(e) {
    e.preventDefault();
    this.props.onToggleEdit();
  }

  editProfile() {
    const { payload } = this.props.homepage;
    const { id } = payload.user;
    this.props.onEditProfileSuling(
      id,
      this.subtitle.value,
      this.location.value,
      this.phone_no.value
    );
  }

  render() {
    const { payload, loading, error, editing } = this.props.homepage;
    const { user } = payload;
    return (
      <div>
        <Helmet>
          <title>Homepage</title>
          <meta name="description" content="Description of Homepage" />
        </Helmet>
        <Hero />
        <MainWrapper
          variables={user ? variables : { ...variables, headerMarginTop: 0 }}
          className="container main"
        >
          <div className="row main__header">
            {loading ? (
              <div className="container">{loading}</div>
            ) : error ? (
              <div>{error}</div>
            ) : (
              <div className="container">
                <div className="row main__profile-pic justify-content-center mb-4">
                  <Image
                    className="profile"
                    src={user.profpic_url || UserPlaceholderImage}
                    alt="Profile Picture"
                    circled
                  />
                </div>
                <div className="row main__name justify-content-center">
                  <h1 className="text-center">
                    {user.user_mahasiswa.nama_lengkap}
                  </h1>
                </div>
                <div className="row main__subtitle text-center justify-content-center mb-2">
                  {editing ? (
                    <div className="col">
                      <input
                        ref={(e) => {
                          this.subtitle = e;
                        }}
                        type="text"
                        className="form-control text-center mt-2"
                        defaultValue={user.subtitle}
                        placeholder="Subtitle"
                      />
                    </div>
                  ) : (
                    user.subtitle
                  )}
                </div>
                <div className="row main__location text-center justify-content-center mb-3">
                  {editing ? (
                    <div className="col">
                      <input
                        ref={(e) => {
                          this.location = e;
                        }}
                        type="text"
                        className="form-control text-center mt-2"
                        defaultValue={user.location}
                        placeholder="Location"
                      />
                    </div>
                  ) : (
                    user.location
                  )}
                </div>
                <div className="row main__location text-center justify-content-center mb-3">
                  {editing ? (
                    <div className="col">
                      <input
                        ref={(e) => {
                          this.phone_no = e;
                        }}
                        type="text"
                        className="form-control text-center mt-2"
                        defaultValue={user.phone_no}
                        placeholder="Phone No"
                      />
                    </div>
                  ) : null}
                </div>
              </div>
            )}
          </div>
          {user && (
            <div className="main__toolbar">
              {editing ? (
                <Button onClick={this.editProfile} className="main__item">
                  Save
                </Button>
              ) : null}
              <a className="main__item" href="#" onClick={this.onToggleEdit}>
                {editing ? (
                  <SlClose size={30} color="gray" />
                ) : (
                  <SlPencil size={30} color="gray" />
                )}
              </a>
            </div>
          )}
          {user && (
            <div className="row main__body">
              <div className="container">
                <div className="row main__exp">
                  <div className="container-fluid">
                    <div className="card bg-white-grey">
                      <div className="card-body">
                        <div className="container">
                          <div className="row">
                            <h4 className="main__title">Experiences</h4>
                          </div>
                          {user.experiences &&
                            user.experiences.map((exp) => (
                              <div
                                className="row main__item p-3 mt-3"
                                key={`${exp.title}${exp.organization}`}
                              >
                                <div className="col-lg-auto main__logo mb-lg-0 mb-4 text-center">
                                  <Image
                                    circled
                                    alt="company-logo"
                                    src={CompanyPlaceholderImage}
                                  />
                                </div>
                                <div className="col-lg-7 text-lg-left text-center">
                                  <div className="row main__title">
                                    <div className="col">
                                      {exp.organization}
                                    </div>
                                  </div>
                                  <div className="row main__position mb-lg-0 mb-2">
                                    <div className="col">{exp.title}</div>
                                  </div>
                                  <div className="row main__description">
                                    <div className="col">{exp.description}</div>
                                  </div>
                                </div>
                                <div className="col mt-lg-0 mt-3">
                                  <div className="row">
                                    <div className="col-3 col-lg-12 text-right">
                                      <small>start from</small>
                                    </div>
                                    <div className="col-3 col-lg-12 text-left text-lg-right main__date">
                                      {exp.start_date}
                                    </div>
                                    <div className="col-3 col-lg-12 text-right">
                                      <small>until</small>
                                    </div>
                                    <div className="col-3 col-lg-12 text-left text-lg-right main__date">
                                      {exp.end_date}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            ))}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          {user && (
            <div className="row main__body mt-3">
              <div className="container">
                <div className="row main__exp">
                  <div className="container-fluid">
                    <div className="card bg-white-grey">
                      <div className="card-body">
                        <div className="container">
                          <div className="row">
                            <h4 className="main__title">Skills</h4>
                          </div>
                          <div className="row">
                            {user.skills &&
                            user.skills.map((skill) => (
                              <div key={`${skill.title}${skill.id}`} className="badge-success badge mr-2">{skill.title}</div>
                            ))}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

        </MainWrapper>
      </div>
    );
  }
}

Homepage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  homepage: PropTypes.shape({
    payload: PropTypes.shape({
      user: PropTypes.shape({
        user_mahasiswa: PropTypes.object,
      }),
    }),
    loading: PropTypes.string,
    error: PropTypes.string,
    editing: PropTypes.bool,
  }),
  onGetProfileSuling: PropTypes.func,
  onGetAuthState: PropTypes.func,
  onToggleEdit: PropTypes.func,
  onEditProfileSuling: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  homepage: makeSelectHomepage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onGetProfileSuling: (userId) => dispatch(getProfileSuling(userId)),
    onEditProfileSuling: (id, subtitle, location, phoneNo) =>
      dispatch(editProfileSuling(id, subtitle, location, phoneNo)),
    onToggleEdit: () => dispatch(toggleEdit()),
    onGetAuthState: () => dispatch(getAuthState()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'app_mahasiswa/homepage', reducer });
const withSaga = injectSaga({ key: 'app_mahasiswa/homepage', saga });

const withAuthReducer = injectReducer({
  key: 'app_auth',
  reducer: authReducer,
});
const withAuthSaga = injectSaga({ key: 'app_auth', saga: authSaga });

export default compose(
  withReducer,
  withSaga,
  withAuthReducer,
  withAuthSaga,
  withConnect
)(Homepage);

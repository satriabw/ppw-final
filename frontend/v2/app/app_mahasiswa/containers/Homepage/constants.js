/*
 *
 * Homepage constants
 *
 */

export const DEFAULT_ACTION = 'app/app_mahasiswa/Homepage/DEFAULT_ACTION';
export const GET_PROFILE_SULING_REQUESTED = 'app/app_mahasiswa/Homepage/GET_PROFILE_SULING_REQUESTED';
export const GET_PROFILE_SULING_SUCCESS = 'app/app_mahasiswa/Homepage/GET_PROFILE_SULING_SUCCESS';
export const GET_PROFILE_SULING_ERROR = 'app/app_mahasiswa/Homepage/GET_PROFILE_SULING_ERROR';
export const EDIT_PROFILE_SULING_REQUESTED = 'app/app_mahasiswa/Homepage/EDIT_PROFILE_SULING_REQUESTED';
export const EDIT_PROFILE_SULING_SUCCESS = 'app/app_mahasiswa/Homepage/EDIT_PROFILE_SULING_SUCCESS';
export const EDIT_PROFILE_SULING_ERROR = 'app/app_mahasiswa/Homepage/EDIT_PROFILE_SULING_ERROR';
export const TOGGLE_EDIT = 'app/app_mahasiswa/Homepage/TOGGLE_EDIT';
export const GET_AUTH_STATE = 'app/app_mahasiswa/Homepage/GET_AUTH_STATE';
export const SET_AUTH_STATE = 'app/app_mahasiswa/Homepage/SET_AUTH_STATE';

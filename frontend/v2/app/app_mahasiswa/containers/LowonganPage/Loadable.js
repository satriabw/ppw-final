/**
 *
 * Asynchronously loads the component for LowonganPage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});

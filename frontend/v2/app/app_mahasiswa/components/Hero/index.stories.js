import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import Hero from './index';

storiesOf('Hero', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic Hero component.`)(() => (
    <Hero>

    </Hero>
  ))
);

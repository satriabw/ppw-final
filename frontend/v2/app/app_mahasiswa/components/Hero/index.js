/**
 *
 * Hero
 *
 */

import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  background: url(${props => props.url_img}) center center;
  background-size: cover;
  height: 30em;
  width: 100%;
`;

class Hero extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    return <Wrapper url_img="https://picsum.photos/200" />;
  }
}

Hero.propTypes = {};

export default Hero;

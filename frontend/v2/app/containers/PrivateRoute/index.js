/**
 *
 * PrivateRoute
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Route, Redirect } from 'react-router-dom';

import { makeSelectUser, makeSelectAuth } from 'containers/App/selectors';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPrivateRoute from './selectors';
import reducer from './reducer';
import saga from './saga';

export class PrivateRoute extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    const { component: Component, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={(props) =>
          1 !== 0 ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: '/auth',
                state: { from: props.location },
              }}
            />
          )
        }
      />
    );
  }
}

PrivateRoute.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  privateroute: makeSelectPrivateRoute(),
  user: makeSelectUser(),
  auth: makeSelectAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'privateRoute', reducer });
const withSaga = injectSaga({ key: 'privateRoute', saga });

export default compose(withReducer, withSaga, withConnect)(PrivateRoute);

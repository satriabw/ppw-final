/**
 *
 * Route
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Redirect, Route as RawRoute } from "react-router-dom";

import {
  makeSelectAuth,
  makeSelectUser,
  makeSelectIsAuthenticated,
  makeSelectLoading
} from "containers/App/selectors";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectRoute from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import { verifyAuth } from "containers/App/actions";

export const AUTH_MODE = {
  REQUIRED: "required",
  NOT_REQUIRED: "not_required",
  REDIRECT: "redirect",
  REDIRECT_WHEN_AUTHENTICATED: "redirect_when_authenticated"
};
let redirectToPath = "";
export class Route extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  isAuthenticated() {
    const { authMode, redirectTo, isAuthenticated, loading } = this.props;
    if (authMode === AUTH_MODE.REQUIRED) {
      redirectToPath = "/auth";
      return loading || isAuthenticated;
    } else if (authMode === AUTH_MODE.REDIRECT) {
      redirectToPath = redirectTo;
      return false;
    } else if (authMode === AUTH_MODE.REDIRECT_WHEN_AUTHENTICATED) {
      redirectToPath = redirectTo;
      return loading || !isAuthenticated;
    } else {
      return true;
    }
  }

  componentWillMount() {
    this.props.onVerifyAuth();
  }

  render() {
    const { component: Component, layout: Layout, user, ...rest } = this.props;
    const RenderComponent = props =>
      Layout ? (
        <Layout user={user}>
          <Component {...props} />
        </Layout>
      ) : (
        <Component {...props} />
      );
    return (
      <RawRoute
        {...rest}
        render={props =>
          this.isAuthenticated() ? (
            RenderComponent(props)
          ) : (
            <Redirect
              to={{
                pathname: redirectToPath,
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  }
}

Route.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  customroute: makeSelectRoute(),
  user: makeSelectUser(),
  auth: makeSelectAuth(),
  isAuthenticated: makeSelectIsAuthenticated(),
  loading: makeSelectLoading()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onVerifyAuth: () => dispatch(verifyAuth())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "customroute", reducer });
const withSaga = injectSaga({ key: "customroute", saga });

export default compose(withReducer, withSaga, withConnect)(Route);

import { call, put, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';
import { VERIFY_AUTH_REQUESTED } from 'containers/App/constants';
import { receiveVerifyAuthFailed, receiveVerifyAuthSuccess, setLoading } from 'containers/App/actions';

import Cookies from 'universal-cookie';

export function* requestVerifyAuth() {
  const cookies = new Cookies();
  const token = cookies.get("jwt_");
  const header = { Authorization: `JWT ${token}` };
  const requestURL = `${process.env.BE_URL}auth/verify/`;
  try {
    const response = yield call(request, requestURL, {
      headers: header
    });
    yield put(receiveVerifyAuthSuccess(response.data));
  } catch (err) {
    yield put(receiveVerifyAuthFailed(err));
  }
}

export default function* defaultSaga() {
  yield [takeLatest(VERIFY_AUTH_REQUESTED, requestVerifyAuth)];
}

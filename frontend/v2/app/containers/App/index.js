/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled, { ThemeProvider } from 'styled-components';
import { Switch, Redirect } from 'react-router-dom';

import MahasiswaRouteApp from 'app_mahasiswa/containers/App';
import AuthRouteApp from 'app_auth/containers/App';
import AdminRouteApp from 'app_admin/containers/App';
import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Route, { AUTH_MODE } from 'containers/Route';
import Navbar from 'components/Navbar';
import Sidebar from 'components/Sidebar';
import theme from 'styles/theme.runtime.scss';
import { SlHome, SlMagnifier, SlUser } from 'components/Icon/sl';

const Wrapper = styled.div`
  padding: 0;
  #wrapper {
    background-color: ${(props) => props.theme.colorBluishWhite};
  }

  @media (max-width: 991.99px) {
    #wrapper {
      margin-left: 0;
      margin-right: 0;
    }

    #main-wrapper {
      padding-top: 0 !important;
    }
  }

  @media (min-width: 992px) {
    #wrapper {
      min-height: 100%;
      height: 100%;
      width: 100%;
      position: absolute;
      top: 0;
      left: 0;
      display: block;
      margin: 0;
      padding-top: ${(props) => props.theme.navbarHeight};
    }

    #main-wrapper {
      height: 100%;
      overflow-y: auto;
    }

    #main {
      position: relative;
      height: 100%;
      overflow-y: auto;
    }

    #sidebar-wrapper {
      height: 100%;
      padding: 0;
      position: fixed;
      border-right: 1px solid ${(props) => props.theme.colorPinkishGrey};
      background-color: white;
    }

    #sidebar {
      position: relative;
      height: 100%;
      overflow: hidden;
    }
  }
`;

const CompanyLayout = (props) => (
  <Wrapper className="container-fluid">
    <Navbar />
    <div id="wrapper" className="row">
      <div id="sidebar-wrapper" className="col-lg-3">
        <div id="sidebar" className="col-auto">
          <Sidebar />
        </div>
      </div>
      <div id="main-wrapper" className="col-lg-9 float-right">
        <div id="main">{props.children}</div>
      </div>
    </div>
  </Wrapper>
);

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <div>
        <Helmet titleTemplate="%s - Suling" defaultTitle="Suling App">
          <meta
            name="description"
            content="App for connecting alumni and its former university."
          />
        </Helmet>
        <Switch>
          <Route authMode={AUTH_MODE.REQUIRED} path="/mahasiswa" component={MahasiswaRouteApp} />
          <Route authMode={AUTH_MODE.REDIRECT_WHEN_AUTHENTICATED} redirectTo="/mahasiswa" path="/auth" component={AuthRouteApp} />
          <Route authMode={AUTH_MODE.REQUIRED} path="/admin" component={AdminRouteApp} />
          <Route authMode={AUTH_MODE.REQUIRED}
            path="/company"
            component={FeaturePage}
            layout={CompanyLayout}
          />
          <Route path="" component={NotFoundPage} />
        </Switch>
      </div>
    </ThemeProvider>
  );
}

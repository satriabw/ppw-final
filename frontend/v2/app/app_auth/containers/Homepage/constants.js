/*
 *
 * Homepage constants
 *
 */

export const DEFAULT_ACTION = 'app/Homepage/DEFAULT_ACTION';
export const REQUEST_LOGIN_CS_AUTH = 'app/HOMEPAGE/REQUEST_LOGIN_CS_AUTH';
export const REQUEST_LOGIN_CS_AUTH_FAILED = 'app/HOMEPAGE/REQUEST_LOGIN_CS_AUTH_FAILED';
export const REQUEST_LOGIN_CS_AUTH_SUCCESS = 'app/HOMEPAGE/REQUEST_LOGIN_CS_AUTH_SUCCESS';
export const REQUEST_SIGN_UP_SULING = 'app/Homepage/REQUEST_SIGN_UP_SULING';
export const REQUEST_SIGN_UP_SULING_FAILED = 'app/Homepage/REQUEST_SIGN_UP_SULING_FAILED';
export const REQUEST_SIGN_UP_SULING_SUCCESS = 'app/Homepage/REQUEST_SIGN_UP_SULING_SUCCESS';

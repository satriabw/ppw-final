import { takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { push } from 'react-router-redux';
import {
  signUpSulingFailed,
  signUpSulingSuccess,
} from './actions';
import {
  REQUEST_SIGN_UP_SULING,
} from './constants';
import {
  REQUEST_LOGIN_CS_AUTH,
} from 'containers/App/constants';
import {
  loginCSAuthFailed,
  loginCSAuthSuccess,
  receiveVerifyAuthSuccess,
} from 'containers/App/actions';

export function* postLoginCSAuth(action) {
  try {
    const {
      username,
      password,
      requestUrl,
    } = action.payload;
    const response = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username,
        password,
      }),
    });

    yield put(loginCSAuthSuccess(response.data));
    yield put(receiveVerifyAuthSuccess(response.data));
  } catch (err) {
    yield put(loginCSAuthFailed(err));
  }
}

export function* postSignUpSuling(action) {
  try {
    const requestUrl = `${process.env.BE_URL}register/`;

    const {
      username,
      password,
      email,
      redirectTo,
    } = action.payload;

    const response = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username,
        password,
        email,
      }),
    });

    yield put(signUpSulingSuccess(response.data));
    if (redirectTo) {
      yield put(push(redirectTo));
    }
  } catch (err) {
    yield put(signUpSulingFailed(err));
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield [
    takeLatest(REQUEST_LOGIN_CS_AUTH, postLoginCSAuth),
    takeLatest(REQUEST_SIGN_UP_SULING, postSignUpSuling),
  ];
}

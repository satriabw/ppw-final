/*
 *
 * Homepage actions
 *
 */
import Cookies from 'universal-cookie';
import {
  DEFAULT_ACTION,
  REQUEST_LOGIN_CS_AUTH,
  REQUEST_LOGIN_CS_AUTH_FAILED,
  REQUEST_LOGIN_CS_AUTH_SUCCESS,
  REQUEST_SIGN_UP_SULING,
  REQUEST_SIGN_UP_SULING_FAILED,
  REQUEST_SIGN_UP_SULING_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function postLoginCSAuth(username, password, requestUrl) {
  return {
    type: REQUEST_LOGIN_CS_AUTH,
    payload: {
      username,
      password,
      requestUrl,
    },
  };
}

export function loginCSAuthFailed(payload) {
  return {
    type: REQUEST_LOGIN_CS_AUTH_FAILED,
    payload: {
      payload,
    },
  };
}

export function loginCSAuthSuccess(payload) {
  const { jwt, user_id } = payload;
  const cookies = new Cookies();
  cookies.set('jwt_', jwt, { path: '/' });
  cookies.set('user_id', user_id, { path: '/' });
  return {
    type: REQUEST_LOGIN_CS_AUTH_SUCCESS,
    payload: {
      payload,
    },
  };
}

export function postSignUpSuling(username, email, password) {
  return {
    type: REQUEST_SIGN_UP_SULING,
    payload: {
      username,
      email,
      password,
    },
  };
}

export function signUpSulingFailed(payload) {
  return {
    type: REQUEST_SIGN_UP_SULING_FAILED,
    payload: {
      payload,
    },
  };
}

export function signUpSulingSuccess(payload) {
  return {
    type: REQUEST_SIGN_UP_SULING_SUCCESS,
    payload: {
      payload,
    },
  };
}

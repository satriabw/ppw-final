import { takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import Cookies from 'universal-cookie';


import {
  getRegisteredUserSuccess,
  getRegisteredUserFail,
  getRegisteredUserStart,
  verifyUserSuccess,
  verifyUserFail,
} from './actions';

import {
  GET_REGISTERED_USER_START,
  POST_VERIFY_USER_START,
} from './constants';

export function* getRegisteredUser() {
  const url = 'http://localhost:8000/register/list/';
  const cookies = new Cookies();
  const token = cookies.get('jwt_');
  const header = { Authorization: `JWT ${token}` };
  try {
    const response = yield call(request, url, {
      method: 'GET',
      headers: header,
    });
    yield put(getRegisteredUserSuccess(response));
  } catch (err) {
    yield put(getRegisteredUserFail(err));
  }
}

export function* veryfyUser(action) {
  console.log('bbb', action);
  const { id } = action.payload;
  const url = `http://localhost:8000/dashboard/registration/mahasiswa/${id}/verify/`;
  const cookies = new Cookies();
  const token = cookies.get('jwt_');
  const header = { Authorization: `JWT ${token}` };
  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: header,
      body: JSON.stringify(''),
    });
    yield put(verifyUserSuccess(response));
    yield put(getRegisteredUserStart());
  } catch (err) {
    yield put(verifyUserFail(err));
  }
}

export default function* defaultSaga() {
  yield [
    takeLatest(GET_REGISTERED_USER_START, getRegisteredUser),
    takeLatest(POST_VERIFY_USER_START, veryfyUser),
  ];
}

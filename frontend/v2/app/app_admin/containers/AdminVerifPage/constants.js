/*
 *
 * AdminVerifPage constants
 *
 */

export const DEFAULT_ACTION = 'app/AdminVerifPage/DEFAULT_ACTION';
export const GET_REGISTERED_USER_START = 'app/AdminVerifPage/GET_REGISTERED_USER_START';
export const GET_REGISTERED_USER_SUCCESS = 'app/AdminVerifPage/GET_REGISTERED_USER_SUCCESS';
export const GET_REGISTERED_USER_FAIL = 'app/AdminVerifPage/GET_REGISTERED_USER_FAIL';

export const POST_VERIFY_USER_START = 'app/AdminVerifPage/POST_VERIFY_USER_START';
export const POST_VERIFY_USER_SUCCESS = 'app/AdminVerifPage/POST_VERIFY_USER_SUCCESS';
export const POST_VERIFY_USER_FAIL = 'app/AdminVerifPage/POST_VERIFY_USER_FAIL';


import { takeLatest, call, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';

import request from 'utils/request';

import {
  loginSuccess,
  loginError,
} from './actions';

import {
  POST_LOGIN_START,
} from './constants';

export function* postLogin(action) {
  const url = 'http://localhost:8000/auth/admin/login/';
  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(action.payload),
    });
    yield put(loginSuccess(response));
    yield put(push('/admin'));
  } catch (err) {
    yield put(loginError(err));
  }
}

export default function* defaultSaga() {
  yield [
    takeLatest(POST_LOGIN_START, postLogin),
  ];
}

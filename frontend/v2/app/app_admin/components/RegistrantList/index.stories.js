import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import RegistrantList from './index';

storiesOf('RegistrantList', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic RegistrantList component.`)(() => (
    <RegistrantList>

    </RegistrantList>
  ))
);

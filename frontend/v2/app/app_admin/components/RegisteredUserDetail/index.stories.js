import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import RegisteredUserDetail from './index';

storiesOf('RegisteredUserDetail', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic RegisteredUserDetail component.`)(() => (
    <RegisteredUserDetail>

    </RegisteredUserDetail>
  ))
);

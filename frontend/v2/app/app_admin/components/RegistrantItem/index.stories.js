import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import RegistrantItem from './index';

storiesOf('RegistrantItem', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic RegistrantItem component.`)(() => (
    <RegistrantItem>

    </RegistrantItem>
  ))
);

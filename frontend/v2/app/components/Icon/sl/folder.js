
import React from 'react';
import Icon from 'react-icon-base';

const SlFolder = props => (
    <Icon viewBox="0 0 1024 1024" {...props}>
        <g><path transform="scale(1, -1) translate(0, -960)" d="M354.752 784l78.624 -77.248l20.1123 -18.752h506.512v-576h-896v672h288h2.7517zM384 848h-320c-35.3438 0 -64 -28.6562 -64 -64v-672c0 -35.3438 28.6562 -64 64 -64h896c35.3438 0 64 28.6562 64 64v576c0 35.3438 -28.6562 64 -64 64h-480z"/></g>
    </Icon>
);

export default SlFolder;

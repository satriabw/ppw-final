
import React from 'react';
import Icon from 'react-icon-base';

const SlArrowDown = props => (
    <Icon viewBox="0 0 1024 1024" {...props}>
        <g><path transform="scale(1, -1) translate(0, -960)" d="M8.2002 684.6c0 8.59961 3.40039 17.4014 10 24.001c13.2002 13.2002 34.7998 13.2002 48 0l451.8 -451.8l445.2 445.2c13.2002 13.2002 34.7998 13.2002 48 0s13.2002 -34.7998 0 -48l-469.2 -469.4c-13.2002 -13.2002 -34.7998 -13.2002 -48 0l-475.8 475.8 c-6.7998 6.7998 -10 15.3994 -10 24.199z"/></g>
    </Icon>
);

export default SlArrowDown;

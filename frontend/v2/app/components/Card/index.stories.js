import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import Card from './index';

storiesOf('Card', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic Card component.`)(() => (
    <Card>

    </Card>
  ))
);

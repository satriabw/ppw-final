import React from "react";

import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { text, withKnobs } from "@storybook/addon-knobs";
import { withInfo } from "@storybook/addon-info";

import Button from "./index";

storiesOf("Button", module)
  .addDecorator(withKnobs)
  .add(
    "with some emoji",
    withInfo(``)(() => (
      <Button onClick={action("onClick")}>{text("Text", "Button")}</Button>
    ))
  );
